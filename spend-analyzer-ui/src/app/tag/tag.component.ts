import { Component, OnInit } from '@angular/core';
import { Tag } from '../_model/tag';
import { TagService } from '../_services/tag.service';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

  tags: Tag[] = [];

  constructor(private tagService: TagService) { }

  ngOnInit(): void {
    this.getAllTags();
  }

  add(tag: string): void {
    if (tag == undefined) {
      console.log("cannot add empty tag");
    }
    else{
      this.tagService.add(tag).subscribe(response =>{
        this.getAllTags();
      });
    }
  }

  delete(tag: Tag): void {
    this.tagService.delete(tag).subscribe(response =>{
      this.getAllTags();
    }); 
  }

  getAllTags(): void{
    this.tagService.getAllTags()
    .subscribe((tagArr: Tag[]) => {
        this.tags = tagArr;
      },
      error => {
        console.log('Error occurred while fetching tags');
      }
    );
  }

}
