import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from "../../environments/environment";
import {BillingData} from "../_model/billing_data";

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  // Returns an observable
  upload(file: any): Observable<any> {

    // Create form data
    const formData = new FormData();

    // Store form name as "file" with file data
    formData.append('file', file, file.name);

    // Make http post request over api
    // with formData as req
    return this.http.post(this.baseUrl + '/v1/data/import', formData);
  }

  getData(): Observable<BillingData[]>{
    return this.http.get<BillingData[]>(this.baseUrl + '/v1/data');
  }
}
