import { Component, OnInit } from '@angular/core';
import {FileUploadService} from '../_services/file-upload.service';
import {BillingData} from "../_model/billing_data";




@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})


export class DataComponent implements OnInit {
  file: File | null = null;
  billingData: BillingData[] | undefined;

  constructor(private fileUploadService: FileUploadService) { }

  ngOnInit(): void {
    this.getBillingData();
  }

  onChange(event: any): void {
    this.file = event.target.files[0];
  }

  getBillingData() {
    this.fileUploadService.getData()
      .subscribe((dataArr: BillingData[]) => {
          this.billingData = dataArr;
        },
        error => {
          console.log('Error occurred while fetching billing data');
        }
      );
  }


// OnClick of button Upload
  onUpload(): void {
    this.fileUploadService.upload(this.file).subscribe(
      (event: any) => {
        console.log('Uploaded');
      }
    );
  }
}
