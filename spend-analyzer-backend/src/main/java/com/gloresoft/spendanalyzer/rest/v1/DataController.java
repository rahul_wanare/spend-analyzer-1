package com.gloresoft.spendanalyzer.rest.v1;

import com.gloresoft.spendanalyzer.entities.BillingData;
import com.gloresoft.spendanalyzer.services.DataService;
import com.gloresoft.spendanalyzer.util.CSVUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


@RestController
@RequestMapping("/v1/data")
@Slf4j
public class DataController {

    @Autowired
    private DataService dataService;

    @GetMapping
    public List<BillingData> getAllData(){
        return this.dataService.getAll();
    }

    @PostMapping("/import")
    public void importData(@RequestParam("file") MultipartFile file) throws IOException {

        log.info("Importing file");

        if (file == null) {
            throw new RuntimeException("No file selected for importing data");
        }

        InputStream inputStream = file.getInputStream();

        List<BillingData> billingData = CSVUtil.csvToBillingData(inputStream);
        this.dataService.importData(billingData);

        log.info("File import successful");

    }

    @PutMapping("/bulk")
    public void bulkUpdate(@RequestBody List<BillingData> billingDataList){
        log.info("Updating items in bulk");
        this.dataService.bulkUpdate(billingDataList);
        log.info("Bulk update successful");
    }

    @PutMapping
    public void update(@RequestBody BillingData billingData){
        log.info("Updating item with id {}", billingData.getId());
        this.dataService.update(billingData);
        log.info("Item updated successfully");
    }
}
